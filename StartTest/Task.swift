//
//  Task.swift
//  StartTest
//
//  Created by Nikita Nechyporenko on 06.06.18.
//  Copyright © 2018 Nikita Nechyporenko. All rights reserved.
//

import UIKit

class Task: NSObject {
    
    static func greeting(count: Int) {
        for _ in 0..<count {
            print("Hello world")
        }
    }
    
    static func daysOfTheWeek(countDay: Int){
        let numberDay = countDay % 7
        if numberDay == 1 {
            print("Monday")
        }
        if numberDay == 2 {
            print("Tuesday")
        }
        if numberDay == 3 {
            print("Wednesday")
        }
        if numberDay == 4 {
            print("Thursday")
        }
        if numberDay == 5 {
            print("Friday")
        }
        if numberDay == 6 {
            print("Saturday")
        }
        if numberDay == 7 {
            print("Sunday")
        }
    }
    
    static func inchСonvertToCentimeters(numberOfInches: Int) {
        let countCentimetrInOneInchs = 2.54
        let numberOfInchesDouble = Double(numberOfInches)
        let resalt = countCentimetrInOneInchs * numberOfInchesDouble
        print(numberOfInches, "inch", "=", resalt, "centimetrs")
    }
    
    static func singingSong(numberOfCouplets: Int) {
        var counter = numberOfCouplets
        while counter > 0 {
            print(counter,"bottles of beer on the wall.")
            print(counter, "bottles of beer.")
            print("You take one down, pass it around.")
            counter = counter - 1
        }
    }
}


