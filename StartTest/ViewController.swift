//
//  ViewController.swift
//  StartTest
//
//  Created by Nikita Nechyporenko on 06.06.18.
//  Copyright © 2018 Nikita Nechyporenko. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var inputTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //        print("Hello World")
        let number = Double(inputTextField.text!)!
        //        print(number)
        let intNumber = Int(number)
        //        for _ in 0..<intNumber {
        //            print("Hello world")
        //        }
        //        Task.greeting(count: intNumber)
        //        Task.daysOfTheWeek(countDay: intNumber)
        //        Task.inchСonvertToCentimeters(numberOfInches: intNumber)
        Task.singingSong(numberOfCouplets: intNumber)
    }
}

